Puzzle Sets wrapping xword-dl for GNOME Crosswords

It should come with the flathub version of Crosswords and normally
shouldn't need to be built from scratch.

Here are some hints if you need to do so.

# Building (part 1): Source

First, install xword-dl. It's available in pip and in fedora

```shell
# Generic linux
$ pip install xword-dl

# Fedora
$ dnf install python3-xword-dl
```

Then, it should be buildable with meson.

```shell
$ meson setup _build
$ ninja -C _build
```

Running crosswords with `PUZZLE_SET_PATH=/path/to/_build/puzzle-sets/`
will load them into the game.

# Building (part 2): flatpak extension

Alternatively, you can build the flatpak extension manually.

```shell
flatpak-builder  --force-clean _flatpak/ org.gnome.Crosswords.PuzzleSets.xword-dl.json  --user --install
```

Then run the crosswords flatpak normally.

# Hints on updating to a new version xword-dl


This extension surprisingly finicky to update to a new version for
flatpak. These notes are mostly for me for when I need to update it. 

**NOTE:** flatpak-pip-generator doesn't run out of the box, and needs
a couple python packages manually installed. I use venv in order to
have a working environment to run it.

First, double check the issues and MRs in gitlab to see if there are
outstanding issues.

Next, update the version to match the xword-dl version in
`meson.build`. This may not be necessary if just pulling a later
version from git.

Next, update the SDK to match the released version of Crosswords if
necessary.

Then see if requirements.txt has changed or needs updating from the
upstream repo. Copy the changes into our version.

Once all that is done, run:
```shell
. .venv/bin/activate
~/Projects/flatpak-builder-tools/pip/flatpak-pip-generator  --requirements-file=requirements.txt -o python3-requirements
```
to generate the new `python3-requirements.json` file.

Unfortunately, the requirements.txt file always brings surprises as it
changes, and flatpak-pip-generator isn't able to keep up with it. As
a result, `python3-requirements.json` needs to be manually adjusted
everytime. In particular:
* Many packages need `--ignore-installed` set on the pip3 install
command line

Once done, update the version in meson.build and tag git.
